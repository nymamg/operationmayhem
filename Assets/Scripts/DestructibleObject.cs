﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleObject : MonoBehaviour, IDamageable {

    [SerializeField]
    private int hp;

    [SerializeField]
    private GameObject solidObject = null;

    [SerializeField]
    private GameObject destroyedObject = null;

    [SerializeField]
    private ParticleSystem debrisParticle = null;

    [SerializeField]
    private Collider solidCollider = null;

    private Vector3 lastDamagePosition = Vector3.zero;

    private GameObject particleObject = null;

    public void Die() {
        solidCollider.enabled = false;
        solidObject.SetActive(false);
        destroyedObject.SetActive(true);
    }

    private void playParticle(Vector3 damagePosition) {
        particleObject = ParticlePool.getInstance().getParticle();
        particleObject.transform.position = damagePosition; // new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z);
        particleObject.GetComponent<ParticleSystem>().GetComponent<Renderer>().material = transform.GetChild(0).GetComponent<MeshRenderer>().material;
        particleObject.SetActive(true);
        particleObject.GetComponent<ParticleSystem>().Play();
        StartCoroutine(returnParticle(particleObject));
    }

    private IEnumerator returnParticle(GameObject particleObject) {
        yield return new WaitForSeconds(particleObject.GetComponent<ParticleSystem>().main.duration);
        ParticlePool.getInstance().returnParticle(particleObject);
    }
    public void TakeDamage(int damage, Vector3 damagePosition) {
        if (hp <= 0) {
            return;
        }
        playParticle(damagePosition);
        hp -= damage;
        if (hp <= 0) {
            Die();
            return;
        }

    }

    public void Update() {
        if (Input.GetButtonDown("Jump")) {
            Die();
        }
    }

    private IEnumerator ChangeLayer() {
        yield return new WaitForSeconds(1);
        destroyedObject.layer = 9;
        foreach (var child in destroyedObject.transform) {
            (child as Transform).gameObject.layer = 9;
        }
    }
}
