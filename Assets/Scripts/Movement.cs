﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
	private Rigidbody rigidBody = null;

	[SerializeField]
	private Animator animator = null;

	[SerializeField]
	private float speed = 0;

    private Vector3 moveDirection;

    // Start is called before the first frame update
    void Start()
    {
         
    }

    // Update is called once per frame
    void FixedUpdate() {
        moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
		moveDirection = Vector3.ClampMagnitude(moveDirection, 1);
        rigidBody.MovePosition(transform.position + speed * Time.deltaTime * moveDirection);
		animator.SetFloat("MoveSpeed", moveDirection.magnitude);
    }

    private void Update() {

    }
}
