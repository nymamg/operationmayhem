﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable {

	void Die();

	void TakeDamage(int damage, Vector3 damagePosition);

}
