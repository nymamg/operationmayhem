﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlePool : MonoBehaviour
{
    public static ParticlePool sharedInstance;

    [SerializeField]
    private GameObject pooledParticle;

    private Queue<GameObject> pool;

    // Start is called before the first frame update
    void Start()
    {
        //sharedInstance = new ParticlePool();
        pool = new Queue<GameObject>();
    }
        
    void Awake() {
        sharedInstance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static ParticlePool getInstance() {
        return sharedInstance;
    }

    public GameObject getParticle() {
        if (pool.Count > 0) {
            Debug.Log("pool has a particle");
            GameObject particle = pool.Dequeue();
            return particle;
        }
        else {
            Debug.Log("pool doesn't have a particle");
            GameObject particle = GameObject.Instantiate(pooledParticle);
            return particle;
        }
    }

    public void returnParticle (GameObject returnedParticle) {
        returnedParticle.SetActive(false);
        pool.Enqueue(returnedParticle);
    }
}
