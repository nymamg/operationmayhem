﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class BasicEnemy : MonoBehaviour, IDamageable {

	[SerializeField]
	private NavMeshAgent agent;

	private Transform player;

	[SerializeField]
	private int hp = 20;

	private bool isAlert = false;

	private NavMeshPath path;

	public void Die() {
		Destroy(gameObject);
	}

	public void TakeDamage(int damage, Vector3 damagePosition) {
		if (hp <= 0) {
			return;
		}
		hp -= damage;
		if (hp <= 0) {
			Die();
		}
	}

	// Start is called before the first frame update
	void Start()
    {
		player = GameObject.FindWithTag("Player").transform;
		path = new NavMeshPath();
		StartCoroutine(WaitAndRetry());
	}

    // Update is called once per frame
    void Update()
    {
		if (isAlert) {
			if (agent.CalculatePath(player.position, path) && path.status == NavMeshPathStatus.PathComplete) {
				agent.SetPath(path);
			}
		}
    }

	private IEnumerator WaitAndRetry() {
		yield return new WaitForSeconds(1);
		isAlert = true;
	}
}
