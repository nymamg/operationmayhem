﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour, IDamageable
{

	[SerializeField]
	private WeaponData weapon = null;

	[SerializeField]
	private Animator animator = null;

	private Transform bulletSpawn = null;

	private bool canFire = true;

	// Start is called before the first frame update
	void Start()
    {
		EquipWeapon();

	}

    // Update is called once per frame
    void Update()
    {
        
    }

	public void EquipWeapon() {
		bulletSpawn = GameObject.Find("Bullet_Spawn").transform;
	}

	public void FireWeapon() {
		if (canFire) {
			animator.SetTrigger("Knockback");
			canFire = false;
			StartCoroutine(ReadyWeapon(60.0f / weapon.fireRate));
			GameObject projectileObject = GameObject.Instantiate(weapon.projectile);
			projectileObject.GetComponent<Projectile>().Shoot(bulletSpawn.position,
															transform.forward * weapon.projectileSpeed);
		}
	}

	public void Die() {

	}

	public void TakeDamage(int damage, Vector3 damagePosition) {

	}

	private IEnumerator ReadyWeapon(float time) {
		yield return new WaitForSeconds(time);
		canFire = true;
	}
}
