﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aiming : MonoBehaviour
{

	private Camera cam;

    Vector3 defaultLookPoint;
    // Start is called before the first frame update
    void Start() {
        cam = Camera.main;
        defaultLookPoint = new Vector3(Input.GetAxis("RightJoystickHorizontal") * 100, transform.position.y, Input.GetAxis("RightJoystickVertical") * 100);
    }

    // Update is called once per frame
    void Update() {

		/// Joystick aiming
		/// TODO: find a better way to stop the character from looking at the starting position
		/// TODO: NEVER COMMENT YOUR FUCKING CODE

#if JOYSTICK_CONTROL
		Vector3 joystickAimPoint = new Vector3(Input.GetAxis("RightJoystickHorizontal") * 100, transform.position.y, Input.GetAxis("RightJoystickVertical") * 100);
		if (joystickAimPoint.x != defaultLookPoint.x && joystickAimPoint.y != defaultLookPoint.y && joystickAimPoint.z != defaultLookPoint.z) {
			transform.LookAt(joystickAimPoint);
		}
	
#else
		/// Mouse aiming (Only one of the aim methods can be active at the same time.)
		Ray camRay = cam.ScreenPointToRay(Input.mousePosition);
        Plane ground = new Plane(Vector3.up, Vector3.zero);
        float rayLength;

        if (ground.Raycast(camRay, out rayLength)) {
            Vector3 aimPoint = camRay.GetPoint(rayLength);
            transform.LookAt(new Vector3(aimPoint.x, transform.position.y, aimPoint.z));
        }
#endif
	}
	
}
