﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectile : Projectile
{
	[SerializeField]
	private int damage = 1;

	[SerializeField]
	private int knockback = 10;

	private Vector3 velocity = Vector3.zero;

	public override void Die() {
		gameObject.SetActive(false);
	}

	public override void OnImpact(Vector3 impactPosition, IDamageable damageable) {
		if(damageable != null) {
			damageable.TakeDamage(damage, impactPosition);
		}
	}

	public override void Shoot(Vector3 initialPosition, Vector3 initialVelocity) {
		transform.position = initialPosition;
		transform.rotation = Quaternion.LookRotation(initialVelocity);
		velocity = initialVelocity;
	}

	public void Update() {
		if (Physics.Raycast(transform.position, velocity, out RaycastHit hit, velocity.magnitude * Time.deltaTime)) {
			OnImpact(hit.point, hit.collider.GetComponent<IDamageable>());
			var body = hit.collider.GetComponent<Rigidbody>();
			if (body) {
				body.AddForceAtPosition(velocity * knockback, transform.position);
			}
			Debug.Log(hit.collider.name);
			Die();
		}
		else {
			transform.position += velocity * Time.deltaTime;
		}
	}

}
