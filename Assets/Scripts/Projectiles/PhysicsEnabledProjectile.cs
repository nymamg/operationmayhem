﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public abstract class PhysicsEnabledProjectile : Projectile {

	public override void Die() {
		GetComponent<Collider>().enabled = false;
		gameObject.SetActive(false);
	}

	public override void Shoot(Vector3 initialPosition, Vector3 initialVelocity) {
		GetComponent<Collider>().enabled = true;
	}

	public void OnCollisionEnter(Collision collision) {
		OnImpact(collision.GetContact(0).point, collision.collider.GetComponent<IDamageable>());
	}
}
