﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : MonoBehaviour {

	// TODO: add ownership

	/// <summary>
	/// This method is called when the projectile is being fired
	/// </summary>
	public abstract void Shoot(Vector3 initialPosition, Vector3 initialVelocity);

	/// <summary>
	/// This method is called when the projectile hits anything
	/// </summary>
	/// <param name="damageable"> 
	/// The damageable object that the projectile hit. Used to calculate damage and other effects 
	/// </param>
	/// <param name="impactPosition"> 
	/// The position of impact
	/// </param>
	public abstract void OnImpact(Vector3 impactPosition, IDamageable damageable);

	/// <summary>
	/// This method is called when the projectile is being pooled, and no longer active in scene
	/// </summary>
	public abstract void Die();

}
