%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: UpperbodyMask
  m_Mask: 01000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Bone012
    m_Weight: 0
  - m_Path: Bone012/Bone001
    m_Weight: 1
  - m_Path: Bone012/Bone001/Bone002
    m_Weight: 1
  - m_Path: Bone012/Bone001/Bone002/Bone003
    m_Weight: 1
  - m_Path: Bone012/Bone001/Bone004
    m_Weight: 1
  - m_Path: Bone012/Bone001/Bone004/Bone005
    m_Weight: 1
  - m_Path: Bone012/Bone001/Bone004/Bone005/Bone006
    m_Weight: 1
  - m_Path: Bone012/Bone001/Bone004/Bone005/Bone006/Bone007
    m_Weight: 1
  - m_Path: Bone012/Bone001/Bone008
    m_Weight: 1
  - m_Path: Bone012/Bone001/Bone008/Bone009
    m_Weight: 1
  - m_Path: Bone012/Bone001/Bone008/Bone009/Bone010
    m_Weight: 1
  - m_Path: Bone012/Bone001/Bone008/Bone009/Bone010/Bone011
    m_Weight: 1
  - m_Path: Bone012/Bone013
    m_Weight: 0
  - m_Path: Bone012/Bone013/Bone014
    m_Weight: 0
  - m_Path: Bone012/Bone013/Bone014/Bone015
    m_Weight: 0
  - m_Path: Bone012/Bone013/Bone014/Bone015/Bone016
    m_Weight: 0
  - m_Path: Bone012/Bone017
    m_Weight: 0
  - m_Path: Bone012/Bone017/Bone018
    m_Weight: 0
  - m_Path: Bone012/Bone017/Bone018/Bone019
    m_Weight: 0
  - m_Path: Bone012/Bone017/Bone018/Bone019/Bone020
    m_Weight: 0
  - m_Path: Capsule001
    m_Weight: 0
  - m_Path: IK Chain001
    m_Weight: 0
  - m_Path: IK Chain002
    m_Weight: 0
  - m_Path: IK Chain003
    m_Weight: 0
  - m_Path: IK Chain004
    m_Weight: 0
