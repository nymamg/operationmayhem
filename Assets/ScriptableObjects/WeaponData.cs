﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New WeaponData", menuName = "Weapon Data", order = 51)]
public class WeaponData : ScriptableObject {

    [SerializeField]
    private string _weaponName = "";

    [SerializeField]
    private int _damage = 1;

	[SerializeField]
	private int _projectileSpeed = 40;

	[SerializeField]
    private int _fireRate = 40;

	// TODO: Change this to use a better projectile spawning system
	[SerializeField]
	private GameObject _projectile = null;

	public string weaponName { get => _weaponName; set => _weaponName = value; }
	public int damage { get => _damage; set => _damage = value; }
	public int projectileSpeed { get => _projectileSpeed; set => _projectileSpeed = value; }
	public int fireRate { get => _fireRate; set => _fireRate = value; }
	public GameObject projectile { get => _projectile; set => _projectile = value; }
}
